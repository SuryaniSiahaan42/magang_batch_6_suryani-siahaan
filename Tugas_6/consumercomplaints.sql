/*Membuat Query Customer Complain*/

Use Javan;

Select * From consumercomplaints;

/*Jumlah komplain setiap bulan */
SELECT MONTHNAME(Date_Received) AS Month, COUNT("Complaint ID") as Total_Complaint from ConsumerComplaints
GROUP BY Month;

/*Komplain yang memiliki tags ‘Older American’*/
SELECT * FROM ConsumerComplaints
WHERE Tags LIKE '%Older American%';


/*Buat sebuah view yang menampilkan data nama perusahaan, jumlah company response to consumer seperti tabel di bawah*/
CREATE VIEW  consumercomplaintscsv AS
    SELECT Company,
           COUNT(CASE WHEN `Company Response to Consumer`= 'Closed'
               THEN 1 else 0 end) Closed,
           COUNT(CASE WHEN `Company Response to Consumer`= 'Closed with explanation' then 1 else 0 end) 'Close with explanation',
           COUNT(CASE WHEN `Company Response to Consumer`='Closed with non-monetary relief' then 1 else 0 end) 'Closed with non-monetary relief'
FROM consumercomplaints c
GROUP BY Company;
SELECT * FROM consumercomplaintscsv;









